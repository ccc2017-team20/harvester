#Write meta tweets into couchDB, without duplicates
#Date: 18/April/2017
#Author:ccc2017-team20, Ke Shi

import couchdb
import json


#TweetStore
#initializing the database, 
#writing records to the database and
#reading records from the database
#the class initializer will either create a new database or 
#open an existing database if there already is one with the given name.
class TweetStore(object):
    def __init__(self, dbname, url='http://team20:202020@localhost:5984/'):
        try:
            self.server = couchdb.Server(url=url)
            self.db = self.server.create(dbname)
            #self._create_views()
        except couchdb.http.PreconditionFailed:
            self.db = self.server[dbname]
     
    #prevent duplicates             
    def save_tweet(self, tw):
        tw['_id'] = tw['id_str']
        if tw['_id'] in self.db:
            pass
        else:
            self.db.save(tw)

storage = TweetStore('meta_tweets_db')

with open('/Users/kshi1/Desktop/cloud-project/meta_tweets.json', 'r') as f:
    lines = f.readlines()
for line in lines:
    line = json.loads(line)
    storage.save_tweet(line)
