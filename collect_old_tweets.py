import couchdb
import tweepy
import json
from tweepy import OAuthHandler

class TweetStore(object):
    def __init__(self, dbname, url='http://team20:202020@localhost:5984/'):
        try:
            self.server = couchdb.Server(url=url)
            self.db = self.server.create(dbname)
            #self._create_views()
        except couchdb.http.PreconditionFailed:
            self.db = self.server[dbname]
                  
    def save_tweet(self, tw):
        tw['_id'] = tw['id_str']
        if tw['_id'] in self.db:
            pass
        else:
            self.db.save(tw)
            
def get_old_tweets(json_file, db_name):
    consumer_key = 'KFQav3ccc5Z02WuxatMD1lncc'
    consumer_secret = 'hs8eeWkk6lYnRKDrBezwWJf7wGvNxE8Dx6rPoF8rr0IRbbRdiw'
    access_token = '723474276691533824-QJT8hBCMFo2RCah9se24JrTsKb96Ucl'
    access_secret = '15xOjDSVAD7fMpSngN5aLyICo9U6eq3DlQywJU1Ji6qlr'   
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_secret)
    api = tweepy.API(auth)
    
    storage = TweetStore(db_name)
    #get screen names, save to a list, for later iterate
    with open(json_file) as f:
        for line in f:
            jsonline = json.loads(line)
            screenName = jsonline['user']['screen_name']
            print screenName
            try:
                timelines = api.user_timeline(screen_name = screenName, count = 3000, include_rts = True)
                for tweet in timelines:
                    if tweet._json['coordinates'] is not None:
                        #store in couchdb
                        storage.save_tweet(tweet._json)
            except Exception, e:
                pass
            
get_old_tweets('meta_tweets3.json', 'meta_tweets_db')